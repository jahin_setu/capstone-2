#!/usr/bin/env python3

import sqlite3
from flask import Flask, request


app = Flask(__name__)
DATABASE = 'modbus_db.sqlite'

def query(_query):
    """Query a SQLite Database"""
    with sqlite3.connect(DATABASE) as con:
        cur = con.cursor()
        cur.execute(_query)
        fetch = cur.fetchall()
        cur.close()
        con.commit()
    return fetch

def get_tokens():
    result = query(f"SELECT password FROM users")
    tokens = list()
    for item in result:
        tokens.append(str(item).lstrip("('").rstrip("',)"))
    return tokens

def is_valid(token):
    tokens = get_tokens()
    if token in tokens:
        return True
    
def get_ip():
    result = query(f"SELECT ip FROM modbus")
    ips = list()
    for item in result:
        ips.append(str(item).lstrip("('").rstrip("',)"))
    return ips

def get_modbus_data():
    with sqlite3.connect(DATABASE) as con:
        cur = con.cursor()
        cur.execute("SELECT * FROM modbus")
        rows = cur.fetchall()
        column_names = [name[0] for name in cur.description]
        results = list()
        cur.close()
        con.commit()
    for row in rows:
        row_dict = dict(zip(column_names, row))
        results.append(row_dict)
    return results


ips = get_ip()
@app.route(f"/<any({str(ips)[1:-1]}):ip>")
def get(ip):
    tokens = get_tokens()
    auth_token = request.headers.get('Authorization')
    if auth_token not in tokens:
        return {'message': 'Invalid authorization token'}, 401
    modbus_data = get_modbus_data()
    for d in modbus_data:
        if d['ip'] == ip:
            return d

@app.route("/")
def default():
    tokens = get_tokens()
    auth_token = request.headers.get('Authorization')
    if auth_token not in tokens:
        return {'message': 'Invalid authorization token'}, 401
    return get_ip()

if __name__ == '__main__':
    app.run(debug=True)