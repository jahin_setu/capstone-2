# Capstone 2 - Modbus Server, Client and RESTful API Interface

## Installing dependencies

* Clone this repository
*   ```sh
    cd capstone-2

    python3 -m venv .venv
    source .venv/bin/activate
    pip install -r requirements.txt
    ```

## Usage

* [modbus_server.py](modbus_server.py) starts the Modbus Server on localhost *(Port 502)*
* [modbus_client.py](modbus_client.py) is the CLI application to interface with the above Modbus Server
* [modbus_webserver.py](modbus_webserver.py) serves the RESTful API server (available at [modbus_rest.py](modbus_rest.py)) onto the localhost *(Port 8080)*
  * `http://127.0.0.1:8080/`
    * User `ada` with password `lovelace` is already stored in the database. Use the password as the Authorization token.
  * This file can be updated to change the IP or port of the deployment

## Features

### The Servers

* The SQLite3 database updates live with updates to the Modbus server.
* The RESTful API server only interfaces with the database instead of directly connecting to the server.

The above design decisions made it so that neither the Modbus server nor the RESTful API server depends on each other.

* Either one can be updated and as long as the data model is the same, a change to the other application would not be necessary.
* The Modbus server can be isolated from the internet and only a symlink to the database can be made, therefore making RESTful API more secure as it has no direct connection to the server.

### The Client

* Automatically reads the vendor name, product code and version of the Modbus server.
* Robust error checking for the inputs to the Modbus server to ensure invalid values are never passed to the server.
* Color-coded boolean values on the CLI.
* Live updates to the database
  * Database is updates as soon as the coils are updated.


## Extra Feature

The client can handle multiple Modbus servers. It stores each IP as an unique piece and updates to this server replaces the old information with current one.

This allows for the index to have only a list of unique IP addresses, which can then be obtained through the root of the RESTful API interface.

A dynamic route is setup that populates the list of routes based on the IPs in the database so that they can be accessed.

Attached with this repository is a video detailing this feature and how it works.

![](capstone_extra_feature.mp4)
