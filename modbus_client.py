#!/usr/bin/env python3


import os
import sys
import argparse
import sqlite3
from getpass import getpass
from pymodbus.client.sync_diag import ModbusTcpClient


DATABASE = "modbus_db.sqlite"

parser = argparse.ArgumentParser(
        description='Modbus TCP Client',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
parser.add_argument(
    '-i', '--ip',
    help='Modbus server IP address',
    type=ascii,
)
parser.add_argument(
    '-p', '--port',
    help='Modbus server TCP port',
    type=ascii,
)


def init(parser):
    args = parser.parse_args()
    if args.ip == None:
        parser.print_help()
        sys.exit()
    elif args.port == None:
        ip = str(args.ip).replace("'", "")
        return ModbusTcpClient(ip)
    else:
        ip = str(args.ip).replace("'", "")
        port = str(args.port).replace("'", "")
        return ModbusTcpClient(ip, port=int(port))


client = init(parser)
client.connect()
if client.is_socket_open():
    pass
else:
    sys.exit(f'Failed to connect to {client}')


class Style:
    """An ASCII Color Code Class"""

    PURPLE = "\033[95m"
    CYAN = "\033[96m"
    DARKCYAN = "\033[36m"
    BLUE = "\033[94m"
    GREEN = "\033[92m"
    YELLOW = "\033[93m"
    RED = "\033[91m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"
    END = "\033[0m"

s = Style()


def clear():
    """Function to clear terminal screen"""
    os.system("cls" if os.name == "nt" else "clear")


def color_code_boolean_list(boolean_list):
    color_coded_list = list()
    for item in boolean_list:
        if item:
            color_coded_list.append(f"{s.GREEN}True{s.END}")
        else:
            color_coded_list.append(f"{s.RED}False{s.END}")
    return color_coded_list


def identification():
    response = client.read_device_information().information
    for key, value in response.items():
        response[key] = value.decode()
    response['VendorName'] = response.pop(0)
    response['ProductCode'] = response.pop(1)
    response['MajorMinorRevision'] = response.pop(2)
    return response


def read_discrete_input_values():
    response = client.read_discrete_inputs(0, 4)
    return response.bits[0:4]


def read_discrete_output_values():
    response = client.read_coils(0, 4)
    return response.bits[0:4]


def read_analog_input_values():
    response = client.read_input_registers(0, 8)
    return response.registers


def read_analog_output_values():
    response = client.read_holding_registers(0, 8)
    return response.registers


def display_discrete_input_values():
    print("\nDiscrete Input Contacts [R/O]\n")

    discrete_input_vals = color_code_boolean_list(read_discrete_input_values())
    for index, value in enumerate(discrete_input_vals):
        print(f"Contact {index}: {value}")

    input('\nMain menu, press Enter: ')
    menu()


def display_discrete_output_values():
    print("\nDiscrete Output Coil [R/W]\n")

    discrete_output_vals = color_code_boolean_list(read_discrete_output_values())
    for index, value in enumerate(discrete_output_vals):
        print(f"Contact {index}: {value}")

    input('\nMain menu, press Enter: ')
    menu()


def display_analog_input_values():
    print("\nAnalogue Input Register [R/O]\n")

    analog_input_vals = read_analog_input_values()
    for index, value in enumerate(analog_input_vals):
        print(f"Register {index}: {value}")

    input('\nMain menu, press Enter: ')
    menu()


def display_analog_output_values():
    print("\nAnalogue Output Holding Register [R/W]\n")

    analog_output_vals = read_analog_output_values()
    for index, value in enumerate(analog_output_vals):
        print(f"Register {index}: {value}")

    input('\nMain menu, press Enter: ')
    menu()


def display_all_values():
    print("\nDisplay all Discrete and Register values\n")

    discrete_input_vals = color_code_boolean_list(read_discrete_input_values())
    discrete_output_vals = color_code_boolean_list(read_discrete_output_values())
    analog_input_vals = read_analog_input_values()
    analog_output_vals = read_analog_output_values()

    display_discrete_input_vals = ', '.join(str(item) for item in discrete_input_vals)
    display_discrete_output_vals = ', '.join(str(item) for item in discrete_output_vals)

    print("Discrete Input Values    ", display_discrete_input_vals)
    print("Discrete Output Values   ", display_discrete_output_vals)
    print("Analog Input Values      ", analog_input_vals)
    print("Analog Output Values     ", analog_output_vals)

    input('\nMain menu, press Enter: ')
    menu()


def write_to_discrete_output():
    print(
        "\nEnter comma separated\n"
        "List of up to 4 bool values: ",
        end=''
    )
    input_string = input()
    input_string = input_string.replace(" ", "")
    input_list = input_string.split(",")
    output_list = list()
    print()
    for value in input_list:
        if value in ("0", "1"):
            output_list.append(value)
        else:
            print(f"Values such as {value} are not valid, skipping.")
    output_list_int = [eval(value) for value in output_list]
    length = len(output_list_int)
    if length < 4:
        print("Padding with 0s applied to complete the coil")
        output_list_int.extend([0] * (4 - length))
    else:
        print("Coil can only take 4 values, truncating the input to take in the first 4 values")
        output_list_int = output_list_int[:4]
    
    coil_values = output_list_int
    print(f"\nAccepting these values {coil_values}")

    request = client.write_coils(0, coil_values)
    if not request.isError():
        print(f"\nSuccess: {s.GREEN}True{s.END}")
        store_to_db()
        display_discrete_output_values()
    else:
        print(f"\nSuccess: {s.RED}False{s.END}")


def write_to_analog_output():
    print(
        "\nEnter comma separated\n"
        "List of up to 8 integer values: ",
        end=''
    )
    input_string = input()
    input_string = input_string.replace(" ", "")
    input_list = input_string.split(",")
    output_list = list()
    print()
    for value in input_list:
        if value.isdigit() and 0 <= int(value) <= 9:
            output_list.append(value)
        else:
            print(f"Values such as {value} are not valid, skipping.")
    output_list_int = [eval(value) for value in output_list]
    length = len(output_list_int)
    if length < 8:
        print("Padding with 0s applied to complete the register")
        output_list_int.extend([0] * (8 - length))
    else:
        print("Register can only take 8 values, truncating the input to take in the first 8 values")
        output_list_int = output_list_int[:8]
    
    register_values = output_list_int
    print(f"\nAccepting these values {register_values}")

    request = client.write_registers(0, register_values)
    if not request.isError():
        print(f"\nSuccess: {s.GREEN}True{s.END}")
        store_to_db()
        display_analog_output_values()
    else:
        print(f"\nSuccess: {s.RED}False{s.END}")


def query(_query):
    """Query a SQLite Database"""
    with sqlite3.connect(DATABASE) as con:
        cur = con.cursor()
        cur.execute(_query)
        fetch = cur.fetchall()
        cur.close()
        con.commit()
    return fetch


def store_to_db():
    TABLE = "modbus"
    query(f"CREATE TABLE IF NOT EXISTS {TABLE} (ip TEXT, di TEXT, co TEXT, ir TEXT, hr TEXT, UNIQUE(ip) ON CONFLICT REPLACE)")

    di = read_discrete_input_values()
    co = read_discrete_output_values()
    ir = read_analog_input_values()
    hr = read_analog_output_values()

    di_str = ','.join(str(int(item)) for item in di)
    co_str = ','.join(str(int(item)) for item in co)
    ir_str = ','.join(str(item) for item in ir)
    hr_str = ','.join(str(item) for item in hr)
    
    ip_arg = parser.parse_args().ip
    ip_str = str(ip_arg).replace("'", "")

    q = f"INSERT INTO {TABLE} VALUES (?, ?, ?, ?, ?)"
    values = (ip_str, di_str, co_str, ir_str, hr_str)
    with sqlite3.connect(DATABASE) as con:
        cur = con.cursor()
        cur.execute(q, values)
        cur.close()
        con.commit()


def create_user():
    TABLE = "users"
    username = input('Enter a username: ')
    password = getpass('Enter a password: ')

    query(f"CREATE TABLE IF NOT EXISTS {TABLE} (username TEXT, password TEXT, UNIQUE(username) ON CONFLICT REPLACE)")
    q = f"INSERT INTO {TABLE} VALUES (?, ?)"
    values = (username, password)
    with sqlite3.connect(DATABASE) as con:
        cur = con.cursor()
        cur.execute(q, values)
        cur.close()
        con.commit()

    print(f"\nUser {username} and the associated password added")
    input('\nMain menu, press Enter: ')
    menu()


def menu():
    clear()

    identity = identification()
    print(
        f"\nVendor Name  : {s.GREEN}{identity['VendorName']}{s.END}\n"
        f"Product Code : {s.GREEN}{identity['ProductCode']}{s.END}\n"
        f"Revision     : {s.GREEN}{identity['MajorMinorRevision']}{s.END}\n"
    )
    print(
        "-------------------------------------------\n\n"
        "1. Discrete Input Contact values\n"
        "2. Discrete Output Coil values\n"
        "3. Analogue Input Register values\n"
        "4. Analogue Output Holding Register values\n"
        "5. Display all Discrete and Register values\n"
        "6. Write values to the Discrete Output Coils\n"
        "7. Write to the Analogue Output Holding Register\n"
        "8. Create/update a username and password\n"
        "9. Exit/Quit\n"
    )

    selection = input('Select an activity: ')

    match selection:
        case "1":
            display_discrete_input_values()
        case "2":
            display_discrete_output_values()
        case "3":
            display_analog_input_values()
        case "4":
            display_analog_output_values()
        case "5":
            display_all_values()
        case "6":
            write_to_discrete_output()
        case "7":
            write_to_analog_output()
        case "8":
            create_user()
        case "9":
            print('\nGoodbye, the Modbus Server will be there when you return ')
            store_to_db()
            sys.exit()
        case _:
            input('\nNot a valid choice, try again (Press Enter)')
            menu()


def main():
    menu()


if __name__ == '__main__':
    main()
